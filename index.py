from flask import Flask
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import session
from flask import Response
from flask import make_response
from base64 import b64encode
import hashlib
import uuid
from database import Database
from functools import wraps


app = Flask(__name__)


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        g._database = Database()
    return g._database


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.disconnect()


@app.route('/')
def start_page():
    email = None
    if "id" in session:
        email = get_db().get_session(session["id"])

    animals = get_db().get_5_animal()

    if animals is None:
        return render_template('index.html', email=email)
    return render_template('index.html', email=email, animals=animals)


def load_image(id):
    binary_data = get_db().load_picture(id)
    if binary_data is None:
        return Response(status=404)
    else:
        response = make_response(binary_data)
        response.headers.set('Content-Type', 'static/images/')
    return response


@app.route('/login')
def page_login():
    return render_template('login.html')


@app.route('/inscription')
def page_inscription():
    return render_template('inscription.html')


@app.route('/adoption')
def adoption_page():
    if "id" in session:
        return render_template('adoption.html')
    page_not_found("page introuvable")


@app.route('/modif')
def modification_page():
    if "id" in session:
        return render_template('modification.html')
    page_not_found("page introuvable")


@app.route('/liste')
def liste_animaux():
    animals = get_db().get_liste_animal()
    return render_template('liste.html', animals=animals)


@app.route('/<nom>/<id>')
def pageanimal(nom, id):
    animal = get_db().get_page_animal(nom, id)
    if animal is None:
        page_not_found("introuvable")
    else:
        return render_template('animal.html', animal=animal)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.route('/connexion', methods=["POST"])
def log_user():
    email = request.form["courriel"]
    password = request.form["pass"]

    if email == "" or password == "":
        return render_template('index.html',
                               erreur="Veuillez remplir les champs")

    email_dbcon = get_db().get_salt_and_hash(email)
    if email_dbcon is None:
        return render_template('login.html',
                               erreur="Vous n'avez pas de compte")

    salt = email_dbcon[0]
    hashed_password = hashlib.sha512(
        str(password+salt).encode("utf-8")).hexdigest()
    if hashed_password == email_dbcon[1]:
        id_session = uuid.uuid4().hex
        get_db().save_session(id_session, email)
        session["id"] = id_session
        return redirect("/")
    else:
        return redirect("/")


@app.route('/insc', methods=["POST"])
def insc_user():
    prenom = request.form["prenom"]
    nom = request.form["nom"]
    telephone = request.form["telephone"]
    address = request.form["adresse"]
    email = request.form["courriel"]
    password = request.form["pass"]

    if email == "" or password == "":
        return render_template('inscription.html',
                               erreur="Veuillez remplir les champs")

    get_db().cree_usr(nom, prenom, telephone, address, email, password)
    return redirect("/")


@app.route('/modification', methods=["POST"])
def modifcation_compte():
    prenom = request.form["prenom"]
    nom = request.form["nom"]
    telephone = request.form["telephone"]
    addresse = request.form["adresse"]
    password = request.form["pass"]
    print(session["id"])
    sesid = get_db().get_session_id(session["id"])

    if not prenom == "":
        get_db().modif_prenom(sesid, prenom)
    if not nom == "":
        get_db().modif_nom(sesid, nom)
    if not telephone == "":
        get_db().modif_tele(sesid, telephone)
    if not addresse == "":
        get_db().modif_addresse(sesid, addresse)
    if not password == "":
        get_db().modif_password(sesid, password)

    return redirect("/")


@app.route('/adopt', methods=["POST"])
def ajout_anim():
    nom = request.form["name"]
    types = request.form["type"]
    race = request.form["race"]
    age = request.form["age"]
    desc = request.form["description"]
    erreur = "Entrez au moins le type et le nom de l'animal"
    if nom == "" or race == "":
        return render_template('adoption.html', erreur=erreur)

    id = session["id"]
    photo = None

    if "image" in request.files:
        photo = request.files["image"]
    get_db().ajout_anim(id, nom, types, race, age, desc, photo)

    return redirect("/")


def authentication_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not is_authenticated(session):
            return send_unauthorized()
        return f(*args, **kwargs)
    return decorated


@app.route('/logout')
@authentication_required
def logout():
    id_session = session["id"]
    session.pop('id', None)
    get_db().delete_session(id_session)
    return redirect("/")


def is_authenticated(session):
    return "id" in session


if __name__ == '__main__':
    app.run(debug=True)


def send_unauthorized():
    return Response('Could not verify your access level for that URL.\n'
                    'You have to login with proper credentials', 401,
                    {'WWW-Authenticate': 'Basic realm="Login Required"'})


app.secret_key = "(*&*&322387he738220)(*(*22347657"
