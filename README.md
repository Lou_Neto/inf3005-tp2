# INF3005: Travail pratique 2

## Description

Ce projet est une application web pour l'adoption et la mise en adoption d'animaux de compagnie. 

Ce travail est réalisé dans le câdre du cours: INF3005: Programmation web avancée, à la session d'hivers 2018.

Présenté à M. Jacques Berger.

## Auteurs

* Pier-Olivier Decoste (DECP09059005)
* Lou-Gomes Neto (NETL14039105)

Étudiants du groupe 50

## Makefile

Toutes les commandes sont exécutables à la ligne de commande au niveau de la racine du projet. 

## Base de données

Le scripte `companions.sql`, nécéssaire pour construire la base de données, se trouve dans le répertoire `db/`. Il suffit d'entrer la commande: 

```sh
make database
```

pour construire la base de données `db/companions.db`. 

**Attention:** Une fois la base de données créé, cette opération est seulement nécéssaire lorsqu'on désire effacer et recréer la base de données. 

## Exécution

La commande:

```sh
make
```

Permet de lancer l'application.

## Fonctionnement

Une fois lancée, l'application sera accessible à partir d'un fureteur en utilisant l'addresse fournie sur la ligne de commande ou l'adresse `localhost:5000/`.

## Références

* [Le dépôt des exemples du cours, dans lequel nous avons utilisés quelques fonctions de base](https://github.com/jacquesberger/exemplesINF3005)

* [Les gabarits utilisés pour l'esthétique du site](https://startbootstrap.com/template-categories/all/)