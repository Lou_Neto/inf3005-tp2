PRAGMA foreign_keys = ON;

create table users (
    id integer primary key,
    nom varchar(50),
    prenom varchar(50),
    telephone varchar(10),
    adresse varchar(100),
    courriel varchar(100),
    salt varchar(32),
    hash varchar(128)
);

create table animaux (
    animid integer,
    animal_nom varchar(50),
    animal_type varchar(50),
    animal_race varchar(50),
    animal_age integer,
    animal_description mediumtext,
    data blob,
    FOREIGN KEY(animid) REFERENCES users(id)
);


create table sessions (
  id integer,
  id_session varchar(32),
  email varchar(25),
  FOREIGN KEY(id) REFERENCES users(id)
);

insert into users (id, nom, prenom, telephone, adresse, courriel, salt, hash) values (1, 'decoste', 'pier-olivier', '5142223322', 'qc, ileperrot', 'podecoste@gmail.com', 'f86cde4da22b453491b9fc9d689e5244', 'c1518f96f37a8bad1cd24c837f6adda7f2b7c940130ea307704327ac6d2f99aff22deb4ead40b9d0b353c39ea12aef971407950e03662e84d9b1846644216203');
insert into sessions (id, id_session, email) VALUES (1, 'fa9238cea5e2417e965d8131f8a0831a' ,'podecoste@hotmail.com');
--insert into animaux (animid, animal_nom, animal_type, animal_race, animal_age, animal_description, data) values (1, 'jumong', 'chat', 'marocain', 4, 'un chat marocain nomme jumon', "�PNG
--");