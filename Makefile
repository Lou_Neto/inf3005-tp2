export FLASK_APP=index.py

run:
	flask run

database: 
	rm -f db/*.db
	sqlite3 db/companions.db < db/companions.sql

clean:
	rm -f *.pyc