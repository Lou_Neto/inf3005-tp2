import sqlite3
import hashlib
import uuid
from base64 import b64encode


class Database:
    def __init__(self):
        self.connection = None

    def get_connection(self):
        if self.connection is None:
            self.connection = sqlite3.connect('db/companions.db')
        return self.connection

    def disconnect(self):
        if self.connection is not None:
            self.connection.close()

    def cree_usr(self, nom, prenom, tele, adresses, email, password):
        connection = self.get_connection()
        salt = uuid.uuid4().hex
        hashed_password = hashlib.sha512(
            str(password + salt).encode("utf-8")).hexdigest()
        connection.execute(("insert into users(nom, prenom," +
                            "telephone, adresse, courriel, salt, hash)" +
                            " values(?, ?, ?, ?, ?, ?, ?)"),
                           (nom, prenom, tele, adresses,
                            email, salt, hashed_password))
        connection.commit()

    def get_5_animal(self):
        cursor = self.get_connection().cursor()
        cursor.execute(("select animid, animal_nom, animal_type, " +
                        "animal_race, animal_age, animal_description, " +
                        "data from animaux ORDER BY RANDOM() LIMIT 5"))
        animals = cursor.fetchall()
        if animals is None:
            return None
        else:
            return [{"id": animal[0],
                     "nom": animal[1],
                     "type": animal[2],
                     "race": animal[3],
                     "age": animal[4],
                     "desc": animal[5],
                     "photo": b64encode(animal[6])} for animal in animals]

    def get_liste_animal(self):
        cursor = self.get_connection().cursor()
        cursor.execute(("select * from animaux"))
        animals = cursor.fetchall()
        if animals is None:
            return None
        else:
            return [{"id": animal[0],
                     "nom": animal[1],
                     "type": animal[2],
                     "race": animal[3],
                     "age": animal[4],
                     "desc": animal[5],
                     "photo": b64encode(animal[6])} for animal in animals]

    def get_page_animal(self, nom, id):
        cursor = self.get_connection().cursor()
        cursor.execute(("select * from animaux " +
                        "WHERE animid=? AND animal_nom=?"),
                       (id, nom))
        animal = cursor.fetchone()
        if animal is None:
            return None
        else:
            return {"id": animal[0],
                    "nom": animal[1],
                    "type": animal[2],
                    "race": animal[3],
                    "age": animal[4],
                    "desc": animal[5],
                    "photo": b64encode(animal[6])}

    def get_salt_and_hash(self, email):
        cursor = self.get_connection().cursor()
        cursor.execute(("select salt, hash from users where courriel=?"),
                       (email,))
        user = cursor.fetchone()
        if user is None:
            return None
        else:
            return user[0], user[1]

    def ajout_anim(self, id, nom, types, race, age, desc, file_data):
        connection = self.get_connection()
        connection.execute(("insert into animaux(animal_nom," +
                            "animal_type, animal_race, animal_age, " +
                            "animal_description, data, animid)" +
                            " values(?, ?, ?, ?, ?, ?, ?)"),
                           [nom, types, race, age, desc,
                            sqlite3.Binary(file_data.read()), id])
        connection.commit()

    def create_picture(self, pic_id, file_data):
        connection = self.get_connection()
        connection.execute("insert into image(id, data) values(?, ?)",
                           [pic_id, sqlite3.Binary(file_data.read())])
        connection.commit()

    def load_picture(self, pic_id):
        cursor = self.get_connection().cursor()
        cursor.execute(("select data from animaux where id=?"), (pic_id,))
        picture = cursor.fetchone()
        if picture is None:
            return None
        else:
            blob_data = picture[0]
            return blob_data

    def modif_prenom(self, id, prenom):
        connection = self.get_connection()
        connection.execute("update users set prenom = ? where id = ?",
                           (prenom, id,))
        connection.commit()

    def modif_nom(self, id, nom):
        connection = self.get_connection()
        connection.execute("update users set nom = ? where id = ?",
                           (nom, id,))
        connection.commit()

    def modif_tele(self, id, tele):
        connection = self.get_connection()
        connection.execute("update users set telephone = ? where id = ?",
                           (tele, id,))
        connection.commit()

    def modif_addresse(self, id, adresse):
        connection = self.get_connection()
        connection.execute("update users set addresse = ? where id = ?",
                           (adresse, id,))
        connection.commit()

    def modif_password(self, id, hash):
        connection = self.get_connection()
        connection.execute("update users set hash = ? where id = ?",
                           (hash, id,))
        connection.commit()

    def save_session(self, id_session, courriel):
        connection = self.get_connection()
        connection.execute(("insert into sessions(id_session, email) "
                            "values(?, ?)"), (id_session, courriel))
        connection.commit()

    def delete_session(self, id_session):
        connection = self.get_connection()
        connection.execute(("delete from sessions where id_session=?"),
                           (id_session,))
        connection.commit()

    def get_session_id(self, id_session):
        cursor = self.get_connection().cursor()
        cursor.execute(("select id from sessions where id_session=?"),
                       (id_session,))
        data = cursor.fetchone()
        if data is None:
            return None
        else:
            return data[0]

    def get_session(self, id_session):
        cursor = self.get_connection().cursor()
        cursor.execute(("select email from sessions where id_session=?"),
                       (id_session,))
        data = cursor.fetchone()
        if data is None:
            return None
        else:
            return data[0]
